#To convert nested list into single list
b=[]
for i in range(0,len(a)):
    if(type(a[i])==type(1)):
        b.append(a[i])
    else:
	c=a[i][:]
	for j in range(0,len(c)):
	    b.append(c[j])

print(b)

# program to demonstrate conditional operator 
n1,n2  = 10, 20
  
# copy value of a in min if a < b else copy b 
min = n1 if n1 < n2 else n2 
  
print(min) 

#to create a dictionary with key i and value i*i
n=int(input("Enter a number"))
d=dict()
for i in range(1,n+1):
    d[i]=i*i

print(d)

# Function which returns a/b using try catch
def Check(a , b): 
    try: 
        c = ((a+b) / (a-b)) 
    except ZeroDivisionError: 
        print "a/b result in 0"
    else: 
        print c 
  
# Driver program to test above function 
Check(2.0, 3.0) 
Check(3.0, 3.0) 